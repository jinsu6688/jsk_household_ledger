# jsk_household_ledger

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### 추가한 library
```
yarn add sass-loader@^10.1.1
yarn add vue-loader
yarn add vue-router@next --save
```
